DEFAULT_CHAT_FRAME:AddMessage("Buff Checker primed!");
BuffChecker = CreateFrame("Frame", nil);
BuffChecker = {}
BuffChecker.textureName = {
	[1]  = "Spell_Holy_FistOfJustice",					-- [1] Blessing of Might
	[2]  = "Spell_Holy_GreaterBlessingofKings",			-- [1] Greater Blessing of Might
	[3]  = "Spell_Holy_SealOfSalvation",				-- [2] Blessing of Salvation
	[4]  = "Spell_Holy_GreaterBlessingofSalvation",		-- [2] Greater Blessing of Salvation
	[5]  = "Spell_Magic_MageArmor",						-- [3] Blessing of Kings
	[6]  = "Spell_Magic_GreaterBlessingofKings",		-- [3] Greater Blessing of Kings
	[7]  = "Spell_Holy_PrayerOfHealing02",			-- (issue) [4] Blessing of Light
	[8]  = "Spell_Holy_GreaterBlessingofLight",			-- [4] Greater Blessing of Light
	[9]  = "Spell_Holy_WordFortitude",					-- [5] Prayer of Fortitude
	[10] = "Spell_Holy_PrayerOfFortitude",				-- [5] Power Word: Fortitude
	[11] = "Spell_Nature_Regeneration",					-- [6] Mark of the Wild/Gift of the Wild(Rank X)
	[12] = "INV_Potion_93",								-- [7] Elixir of Greater Agility
	[13] = "INV_Potion_32",								-- [7] Elixir of the Mongoose
	[14] = "Spell_Holy_Devotion",						-- [8] Blessed Sunfruit - 10 str
	[15] = "Spell_Holy_PrayerOfHealing02",			-- (issue) [9] Greater Arcane Protection Potion
	[16] = "Spell_Fire_FireArmor",						-- [10] Greater Fire Protection Potion
	[17] = "Spell_Frost_FrostArmor02",					-- [11] Greater Frost Protection Potion
	[18] = "Spell_Nature_SpiritArmor",					-- [12] Greater Nature Protection Potion
	[19] = "Spell_Shadow_RagingScream",					-- [13] Greater Shadow Protection Potion
	[20] = "INV_Drink_04",								-- [14] Rumsey Rum Black Label
	[21] = "INV_Drink_03",								-- [14] Gordok Green Grog
	[22] = "INV_Misc_MonsterScales_11",					-- [15] Juju Power - 30 str
	[23] = "INV_Misc_MonsterScales_15",					-- [16] Juju Ember - 15 fire res
	[24] = "INV_Misc_MonsterScales_09",					-- [17] Juju Chill - 15 frost res
	[25] = "INV_Misc_MonsterScales_07",					-- [18] Juju Might - 40 ap
	[26] = "Spell_Nature_Purge",						-- [19] Lung Juice Cocktail - 25 stam
	[27] = "Spell_Nature_Strength",						-- [20] R.O.I.D.S. - 25 str
	[28] = "Spell_Nature_ForceOfNature",				-- [21] Ground Scorpok Assay - 25 agi
	[29] = "INV_Potion_61",								-- [15] Elixir of Giants - 25str
	[30] = "Spell_MiscFood"								-- [8] Well Fed, Smoked Desert Dumplings
}
BuffChecker.groups = {
	[1]  = "Blessing of Might",
	[2]  = "Blessing of Salvation",
	[3]  = "Blessing of Kings",
	[4]  = "Blessing of Light",
	[5]  = "Power Word: Fortitude",
	[6]  = "Mark of the Wild",
	[7]  = "Elixir of Greater Agility",
	[8]  = "Well Fed, Smoked Desert Dumplings",
	[9]  = "Greater Arcane Protection Potion",
	[10] = "Greater Fire Protection Potion",
	[11] = "Greater Frost Protection Potion",
	[12] = "Greater Nature Protection Potion",
	[13] = "Greater Shadow Protection Potion",
	[14] = "Rumsey Rum Black Label, Gordok Green Grog",
	[15] = "Juju Power, Elixir of Giants",
	[16] = "Juju Ember - 15 fire res",
	[17] = "Juju Chill - 15 frost res",
	[18] = "Juju Might - 40 ap",
	[19] = "Lung Juice Cocktail - 25 stam",
	[20] = "R.O.I.D.S. - 25 str",
	[21] = "Ground Scorpok Assay - 25 agi"
}

function BuffChecker:Check()
	local i = 0;
	local current = {
		[1]  = 0,
		[2]  = 0,
		[3]  = 0,
		[4]  = 0,
		[5]  = 0,
		[6]  = 0,
		[7]  = 0,
		[8]  = 0,
		[9]  = 0,
		[10] = 0,
		[11] = 0,
		[12] = 0,
		[13] = 0,
		[14] = 0,
		[15] = 0,
		[16] = 0,
		[17] = 0,
		[18] = 0,
		[19] = 0,
		[20] = 0,
		[21] = 0,
		[22] = 0,
		[23] = 0,
		[24] = 0,
		[25] = 0,
		[26] = 0,
		[27] = 0,
		[28] = 0,
		[29] = 0,
		[30] = 0,
	}
	while not (GetPlayerBuff(i, "HELPFUL") == -1) do
		local buffIndex, untilCancelled = GetPlayerBuff(i, "HELPFUL")
		local texture = GetPlayerBuffTexture(buffIndex);

		for j = 1,30 do 
		    if (string.find(texture,BuffChecker.textureName[j])) then
				current[j] = 1;
			end
		end

		i = i + 1;
	end

	DEFAULT_CHAT_FRAME:AddMessage("Missing buffs:",0,1,1);
	DEFAULT_CHAT_FRAME:AddMessage("-------------------");
	if (current[1] == 0 and current[2] == 0) then
		DEFAULT_CHAT_FRAME:AddMessage(BuffChecker.groups[1]);
	end
	if (current[3] == 0 and current[4] == 0) then
		DEFAULT_CHAT_FRAME:AddMessage(BuffChecker.groups[2]);
	end
	if (current[5] == 0 and current[6] == 0) then
		DEFAULT_CHAT_FRAME:AddMessage(BuffChecker.groups[3]);
	end
	-- if (current[7] == 0 and current[8] == 0) then
	-- 	DEFAULT_CHAT_FRAME:AddMessage(BuffChecker.groups[4]);
	-- end
	if (current[9] == 0 and current[10] == 0) then
		DEFAULT_CHAT_FRAME:AddMessage(BuffChecker.groups[5]);
	end
	if (current[11] == 0) then
		DEFAULT_CHAT_FRAME:AddMessage(BuffChecker.groups[6]);
	end
	if (current[12] == 0 and current[13] == 0) then
		DEFAULT_CHAT_FRAME:AddMessage(BuffChecker.groups[7]);
	end
	if (current[14] == 0 and current[30] == 0) then
		DEFAULT_CHAT_FRAME:AddMessage(BuffChecker.groups[8]);
	end
	if (current[15] == 0) then
		DEFAULT_CHAT_FRAME:AddMessage(BuffChecker.groups[9]);
	end
	if (current[16] == 0) then
		DEFAULT_CHAT_FRAME:AddMessage(BuffChecker.groups[10]);
	end
	-- if (current[17] == 0) then
	-- 	DEFAULT_CHAT_FRAME:AddMessage(BuffChecker.groups[11]);
	-- end
	if (current[18] == 0) then
		DEFAULT_CHAT_FRAME:AddMessage(BuffChecker.groups[12]);
	end
	if (current[19] == 0) then
		DEFAULT_CHAT_FRAME:AddMessage(BuffChecker.groups[13]);
	end
	if (current[20] == 0 and current[21] == 0) then
		DEFAULT_CHAT_FRAME:AddMessage(BuffChecker.groups[14]);
	end
	if (current[22] == 0 and current[29]) then
		DEFAULT_CHAT_FRAME:AddMessage(BuffChecker.groups[15]);
	end
	-- if (current[23] == 0) then
	-- 	DEFAULT_CHAT_FRAME:AddMessage(BuffChecker.groups[16]);
	-- end
	-- if (current[24] == 0) then
	-- 	DEFAULT_CHAT_FRAME:AddMessage(BuffChecker.groups[17]);
	-- end
	-- if (current[25] == 0) then
	-- 	DEFAULT_CHAT_FRAME:AddMessage(BuffChecker.groups[18]);
	-- end
	-- if (current[26] == 0) then
	-- 	DEFAULT_CHAT_FRAME:AddMessage(BuffChecker.groups[19]);
	-- end
	-- if (current[27] == 0) then
	-- 	DEFAULT_CHAT_FRAME:AddMessage(BuffChecker.groups[20]);
	-- end
	-- if (current[28] == 0) then
	-- 	DEFAULT_CHAT_FRAME:AddMessage(BuffChecker.groups[21]);
	-- end

end