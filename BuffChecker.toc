## Interface: 11200
## Title: Buff Checker
## Version: 0.4
## Author: Sykst
## Notes: Checks for the raid buffs, letting you know if any are missing.

# main
BuffChecker.lua