# BuffChecker #
A very simple addon, which let's you know which raid buffs you are missing.  
Currently it checks for rogue buffs only.  

## Installing ##   
1. Download the folder and rename it into BuffChecker  
2. Make this macro: /script BuffChecker:Check()  
3. Use it  